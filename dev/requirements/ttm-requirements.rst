
.. role:: cpp(code)
  :language: cpp


.. role:: bash(code)
  :language: bash


.. role:: env(code)
  :language: bash


=========================================
TMS - Template Management System
=========================================


:Author: bg (bgraf@uos.de)
:Date: 2017-04-30
:Revision: 1

.. |TMS| replace:: TMS
.. |tms| replace:: :code:`tms`


Introduction
=========================================

.. code:: bash

   $ tms
   $ tms init latex-simple --no-gitignore
   $ tms init ocaml -d new_project
   $ tms init rst-cute
   $ tms list
   $ tms init
   
The |TMS| tool consists of one executable script |tms|.
All functionality will be accessibly through |TMS|'s sub commands.

Functionality
--------------------------

- **Initialize a directory**

  Using the :env:`tms init` sub command, the current or a given directory will
  be populated using a predefined template.
  General and template specific options can be used to influence the
  resulting layout.

  + Include a :env:`.gitignore`
  + Include a :env:`LICENSE` file and choose which one
  + For instance in a CMake project, determine the number of sub libraries
    and sub executables.
  + Create a :env:`Makefile` to perform some standard tasks.

- **List all templates**

  Show a list of all available templates by executing :env:`tms list`.

- **Show the specifics of a given template**

  The sub command :env:`tms show <template-name>` should give a description
  of the template and an explaination for all template-specific options.

- **Show general information**

  The sub command :env:`tms info` should print the current version
  and template-search paths.


Use-cases
--------------------------
     
1. Simple file copying

   Given some set of files.
   If the template is used, these files should be copied to the target
   directory.

2. Simple copying and value substitution

   A set of files is being copied and some files
   are used as input to a templating engine which substitutes some
   values.

3. Copying and renaming

   Given a set of files, some files are simply copied or substituted
   while others are being copied and renamed.


Implementation details
--------------------------


Dependencies
************************************

* Python (>= 3.5)
* Jinja2__
* Appdirs

__ http://jinja.pocoo.org/


Error handling
************************************

The module :env:`tms` will define some exception classes which can be
thrown from callbacks.
The base class for these exceptions should contain a name,
a longer description and a fixit-message, if available.


Process
************************************

#. Read command line parameters

#. Perform subcommand

#. Read config file

#. Read template directories and establish modules

#. Instantiate template logic and set variables/params

#. Fetch arg parser from template logic and parse unknown arguments.




Dynamic module loading
************************************

Given a directory layout like the following:

.. code::

  tms.py
  templates/__init__.py
  templates/template-a/__init__.py
  templates/template-b/__init__.py

The modules in :env:`templates/template-a` and :env:`templates/template-b`
can be loaded in the following way:

.. code:: python

  import importlib
  mod = importlib.import_module('templates.template-a')
  if hasattr(mod, 'logic'):
      templateLogic = mod.logic()
      templateLogic.some_callback()

The file :env:`templates/template-a/__init__.py` contains the following example
code:

.. code:: python

    import tms

    class MyTemplate(tms.Template):
        def __init__(self):
            pass

        def callback(self, param):
            print('MyTemplate.callback:', param)
            super().callback(param)

    logic = MyTemplate


**A**: given some arbitrary source directory, load a module as follows:

For Python 3.5+

.. code:: python

   import importlib.util
   spec = importlib.util.spec_from_file_location("module.name", "/path/to/file.py")
   foo = importlib.util.module_from_spec(spec)
   spec.loader.exec_module(foo)
   foo.MyClass()


For Python 3.3, 3.4

.. code:: python

   from importlib.machinery import SourceFileLoader

   foo = SourceFileLoader("module.name", "/path/to/file.py").load_module()
   foo.MyClass()


Source SO__

__ https://stackoverflow.com/questions/67631/how-to-import-a-module-given-the-full-path/67692#67692

**Q**: How to determine the Python version?

**A**: The :env:`sys` module provides various functions:

.. code:: python

   >>> import sys
   >>> sys.version
   '3.5.2 (default, Nov 17 2016, 17:05:23) \n[GCC 5.4.0 20160609]'
   >>> sys.version_info
   sys.version_info(major=3, minor=5, micro=2, releaselevel='final', serial=0)
   >>> sys.hexversion
   50660080
   >>> 

*=>* :env:`sys.version_info` is the way to go.

**TODO**

- Create a class :env:`LogicLoading` that encapsulates version dependent
  logic.

  The loading **should fail** if there's no :env:`logic` attribute in the module.

  .. code:: python

     getattr(mod, 'logic')

- Use :env:`LogicLoading` to load actual modules.




Sub commands in Python3 and Argparse
*************************************

.. code:: python

    parser = argparse.ArgumentParser()
    parser.add_argument('-g', '--global')
    subparsers = parser.add_subparsers(dest="subparser_name") # this line changed
    foo_parser = subparsers.add_parser('foo')
    foo_parser.add_argument('-c', '--count')
    bar_parser = subparsers.add_parser('bar')
    args = parser.parse_args(['-g', 'xyz', 'foo', '--count', '42'])
    args
    >>> Namespace(count='42', global='xyz', subparser_name='foo')


Template specific argument parsers
**********************************

.. code:: python

    from argparse import ArgumentParser

    parser = ArgumentParser('ok')
    parser.add_argument('--bla', action='store_true')

    argv = '--bla --another 23'.split()
    args, unknown = parser.parse_known_args(argv)

    specific_parser = ArgumentParser('specific_parser')
    specific_parser.add_argument('--another', type=int)
    specific_args = specific_parser.parse_args(unknown)

    print(args)
    print(specific_args)


TemplateLogic
**********************************

The *TemplateLogic* performs the population of the target
directories.
A TemplateLogic can take various additional parameters.

CopySubstituteLogic
~~~~~~~~~~~~~~~~~~~

This is the default logic.
It covers use-cases (1), (2) and (3).


Configuration file format
**********************************

**Q**: What kind of format is well suited?

**A**: YAML is the way to go:

- Allows arbitrary nesting
- Allows comments
- Allows blocks of text (longer descriptions)
- Easy on the eye
- Maps nicely to common data structures, i.e. maps and lists

Thoughts
--------------------------

- Templates should be inheritable and joinable.

- General options provided for all templates:

  + Project name
  + Project author
  + Target directory
  + Template name (source directory)

- Files should be renameable

  For instance, given a latex project using the project name *foo*,
  the file :env:`main.tex` could be renamed to :env:`foo.tex`.

- Questions

  + What templating engine should be used?
  + How can python dynamicall load modules?
  + How to implement file-renaming, e.g. :env:`main.tex` to :env:`<project name>.tex`?
  + What is the template search directory's layout?

Possible templates
*********************

* **Ocaml + Oasis**

  Optionally with :env:`cmdliner`, :env:`lwt`, :env:`containers` or any other package.
  Could have OPAM interaction.

* **CMake for C++**

  Possibly with sub-directories for executables and libraries.

  Optionally with boost, boost version and required packages.

* **CMake for C**

* **LaTeX**

  Optionally with Makefile, .gitignore and .latexmk.

  Also with literature and acronyms.


TODO
--------

* **=>**

  - implement :env:`TemplateLogic`
  - implement :env:`CopyLogic`
  - implement properties to parameterize :env:`CopyLogic`
  

* **Q**: What about modules/mixins?

  There should be some common modules, e.g. a LICENSE-file module
  that can be easily pushed into a template.
  Modules should be like mixins in programming languages like D and Ruby.
  Maybe mixin is a better name than module.

  Default mixins:

  - gitignore (does nothing, just provides a parser flag)
  - license (does copy the selected LICENSE file)
  - readme (does create a README.md with appropriate header)


* **Colorized output**

  Use a package like :env:`colorama` to colorize the output of the
  tool.
  Should be an optional dependency: if it's available,
  then produce colorized output, otherwise don't.

  **Q**: How to check for the terminal's color abilities?
