#!/usr/bin/env python3

# Template Management System
#
# (c) Benjamin Graf, 2017


TMS_APPNAME = 'tms'
TMS_VERSION = '0.0.1-prealpha'

from enum import Enum
from pathlib import Path
import logging
import sys
import yaml, yaml.parser

DEFAULT_LOG_LEVEL = logging.DEBUG

LOG_FMT = logging.Formatter(
    '%(asctime)s [%(name)s] [%(levelname)s] %(message)s')
LOG_HANDLER = logging.StreamHandler(sys.stderr)
LOG_HANDLER.setFormatter(LOG_FMT)
LOGGER = logging.getLogger('tms')
LOGGER.setLevel(DEFAULT_LOG_LEVEL)
LOGGER.addHandler(LOG_HANDLER)

import appdirs


class Paths:
    def __init__(self, config_path, appname=TMS_APPNAME):
        self.__appdirs = appdirs.AppDirs(appname)
        self.__config_path = config_path

    @property
    def config_file(self):
        if self.__config_path is None:
            return self.default_config_file
        else:
            return Path(self.__config_path)

    @property
    def default_config_file(self):
        return Path(self.__appdirs.user_config_dir, 'config.json')

    @property
    def default_template_roots(self):
        return [Path(self.__appdirs.user_config_dir, 'templates'),
                Path(Path(__file__).parent, 'templates')]


class Template:
    def __init__(self, name, dir):
        self.name = name
        self.dir = dir

    @property
    def properties(self):
        return self.dir.properties

    @property
    def title(self):
        return self.__property_or_none('title')

    @property
    def description(self):
        return self.__property_or_none('description')

    def __property_or_none(self, name):
        return self.properties.get(name, None)

    @property
    def path(self):
        return self.dir.path

    def has_logic_file(self):
        logic_file_path = Path(self.path, '__init__.py')
        return logic_file_path.is_file()


class TemplateRepository:
    def __init__(self, template_paths_by_name, actual_paths):
        self.__template_paths_by_name = template_paths_by_name
        self.__paths = actual_paths

    @property
    def paths(self):
        return self.__paths

    @property
    def numof_templates(self):
        return len(self.__template_paths_by_name)

    @property
    def numof_templates_total(self):
        total_number = 0
        for val in self.__template_paths_by_name.values():
            total_number += len(val)
        return total_number

    @property
    def numof_templates_shadowed(self):
        return self.numof_templates_total - self.numof_templates

    def has_template(self, name):
        return name in self.__template_paths_by_name

    __contains__ = has_template

    def template(self, name):
        return self.__template_paths_by_name[name][0]

    def templates_by_name(self):
        for name in sorted(self.__template_paths_by_name.keys()):
            templates = self.__template_paths_by_name[name]
            yield name, templates


class Target:
    def __init__(self, directory_path):
        self.directory_path = Path(directory_path)
        # TODO raise if it exists but is not a directory.

    def directory_exists(self):
        return self.directory_path.exists()

    def ensure_directory(self):
        """ Ensures that the target directory exists.
        
        If the directory doesn't exist, it will be created.
        
        Return:
            True if directory has been created, False if it already existed.
        """
        if not self.directory_exists():
            self.directory_path.mkdir(parents=True, exist_ok=False)
            return True
        else:
            return False

    def relative_path(self, relative_path):
        return Path(self.directory_path, relative_path)


class Context:
    def __init__(self, args):
        self.args = args


class TemplateLogic:
    def __init__(self, template, *args, **kwargs):
        self.__template = template

    @property
    def template(self):
        return self.__template

    def make_parser(self):
        parser = argparse.ArgumentParser(
            'tms init {}'.format(self.template.name)
        )
        return parser

    def create_target(self, *args, **kwargs):
        """ Initializes and populates the target directory.
        """
        pass


def traverse(base_path, follow_symlinks=False):
    path = Path(base_path)
    if not path.is_dir():
        raise ValueError('given base path is not a directory')

    stack = [path]

    while len(stack) > 0:
        path = stack.pop()
        yield path
        for contained_path in path.iterdir():
            if contained_path.is_dir():
                if follow_symlinks or not contained_path.is_symlink():
                    stack.append(contained_path)
            else:
                yield contained_path


class CopyLogic(TemplateLogic):
    def make_parser(self):
        parser = super().make_parser()
        parser.add_argument(
            '-l', '--license',
            type=str,
            default=None,
            const=True,
            nargs='?',
            help='include a LICENSE file')

        parser.add_argument(
            '-t', '--title',
            metavar='TITLE',
            dest='project_title',
            type=str,
            default=None,
            help='name of the project'
        )

        parser.add_argument(
            '--gitignore',
            dest='with_gitignore',
            action='store_true',
            help='include a .gitignore if available'
        )
        return parser

    def create_target(self, context, target, *args, **kwargs):
        target.ensure_directory()

        # assume target_dir exists..
        print(context.args)
        print(target.directory_path)

        for p in traverse(self.template.path):
            print(p)

        pass


class DefaultLogic(CopyLogic):
    pass


class State:
    """ The overall application state.

    This includes:
    - Configurations
    - Command line parameters
    - Directories and search paths
    - The template repository
    """

    def __init__(self, paths, configuration, repository, args, unknown_args):
        self.paths = paths
        self.configuration = configuration
        self.repository = repository
        self.args = args
        self.unkwown_args = unknown_args


class DirectoryType(Enum):
    TEMPLATE = 1
    ROOT = 2
    NAMESPACE = 3
    IGNORE = 3


class NoDirectory(ValueError):
    pass


class MalformedPropertiesFile(ValueError):
    pass


class IllegalPropertyContent(Exception):
    def __init__(self, path, message):
        self.path = path
        self.message = message


class Directory:
    def __init__(self, path, default_type=DirectoryType.TEMPLATE):
        self.path = Path(path)
        if not self.path.exists() or not self.path.is_dir():
            raise NoDirectory(self.path)

        self.__properties = None
        self.type = self.__directory_type() or default_type

    @property
    def properties(self):
        if self.__properties is None:
            self.__properties = self.__read_properties()
        return self.__properties

    @property
    def properties_path(self):
        return Path(self.path, '.tms.yml')

    def __directory_type(self):
        properties = self.properties

        # Check if there is a type indicator in the properties.
        if 'type' in properties:
            type_name = properties['type']
            try:
                dir_type = DirectoryType[type_name.upper()]
                return dir_type
            except KeyError:
                LOGGER.error(
                    'illegal directory type in properties: "%s" at %s',
                    type_name.upper(),
                    self.path
                )
                raise IllegalPropertyContent(
                    self.properties_path,
                    'illegal directory type "{}" in properties'.format(type_name.upper())
                )

        root_file_path = Path(self.path, '.tms-root')
        if root_file_path.is_file():
            return DirectoryType.ROOT

        namespace_file_path = Path(self.path, '.tms-namespace')
        if namespace_file_path.is_file():
            return DirectoryType.NAMESPACE

        ignore_file_path = Path(self.path, '.tms-ignore')
        if ignore_file_path.is_file():
            return DirectoryType.IGNORE

        return None

    def __property_or_none(self, name):
        return self.properties.get(name, None)

    def __read_properties(self):
        properties_path = self.properties_path
        if not properties_path.is_file():
            return {}

        with properties_path.open('r') as f:
            try:
                properties = yaml.load(f)
            except yaml.parser.ParserError as e:
                LOGGER.error(
                    'Malformed properties for directory: %s',
                    self.path
                )
                raise MalformedPropertiesFile(properties_path)

        return properties


def read_configuration(config_path):
    LOGGER.info('Reading configuration file from: %s', str(config_path))
    path = Path(config_path)

    if not path.exists():
        LOGGER.error('Could not read configuration file from %s', path)
        return None

    if not path.is_file():
        LOGGER.error('Configuration file path is not a file at %s', path)
        return None

    raise RuntimeError('NOT IMPLEMENTED')
    return None


def init_repository(paths):
    """ Initialize a template repository.

    Args:
        paths (path list):
            The template search path, i.e. a list of template roots.
    Return:
        A initialized and sound repository.
    """
    template_sources_by_name = {}

    def add_template(dir, name):
        if name not in template_sources_by_name:
            template_sources_by_name[name] = []
        else:
            LOGGER.warning(
                'Template "%s" in %s is being shadowed by a prior template of the same name',
                name,
                dir.path.absolute())
        template_sources_by_name[name].append(Template(name, dir))

    # TODO : handle paths that are definitely templates too,
    #        and namespaces.
    stack = [(Directory(path, default_type=DirectoryType.ROOT), {})
             for path in reversed(paths) if path.is_dir()]

    actual_paths = []

    while len(stack) > 0:
        dir, additional_info = stack.pop()
        LOGGER.info('Reading repository path: %s', dir.path)

        if not dir.path.exists():
            LOGGER.error('Repository dir.path does not exist: %s', dir.path)
            continue

        if dir.path.is_file():
            LOGGER.error('Repository dir.path is file: %s', dir.path)
            continue

        if not dir.path.is_dir():
            LOGGER.error(
                'Repository dir.path is neither a file nor a directory: %s',
                dir.path)
            continue

        # TODO check if there is a config or ignore file

        # Path denotes a directory and is a template root,
        # therefore treat every sub directory as a potential
        # template directory.

        actual_paths.append(dir)

        for contained_path in dir.path.iterdir():
            if contained_path.is_file():
                LOGGER.info(
                    'Ignoring file in template root: %s',
                    contained_path)
                continue

            if contained_path.is_dir():
                contained_dir = Directory(contained_path)
                dir_type = contained_dir.type

                if dir_type == DirectoryType.TEMPLATE:
                    template_name = '/'.join(
                        additional_info.get('namespaces', []) + [contained_path.name]
                    )
                    add_template(contained_dir, template_name)
                    LOGGER.debug(
                        'Adding template "%s" from: %s',
                        template_name,
                        contained_path)

                elif dir_type == DirectoryType.ROOT:
                    LOGGER.info(
                        'Adding recursive template root: %s',
                        contained_path)
                    stack.append((contained_dir, additional_info))

                elif dir_type == DirectoryType.NAMESPACE:
                    new_additional_info = dict(**additional_info)
                    if 'namespaces' not in new_additional_info:
                        new_additional_info['namespaces'] = []
                    new_additional_info['namespaces'].append(contained_path.name)
                    stack.append((contained_dir, new_additional_info))

    return TemplateRepository(template_sources_by_name, actual_paths)


def init_state(paths, args, unknown_args):
    """ Initialize the overall state of the application.

    Args:
        paths
            The paths collection
        args
            Dictionary of command line parameters.
        unknown_args
            List of unknown arguments.
    Return:
        State object.
    """
    configuration = read_configuration(paths.config_file)
    if configuration is None:
        configuration = {
            'path': paths.default_template_roots
        }

    repository = init_repository(configuration['path'])

    return State(paths, configuration, repository, args, unknown_args)


# Imports required by executable
if __name__ == '__main__':
    import argparse


def print_heading(message):
    print('=' * 79)
    print(message.center(79))
    print('=' * 79)


def print_info_key(key, extra=None):
    extra_message = '[{}]'.format(extra) if extra is not None else ''
    print('{}: {}'.format(key, extra_message))


def print_info_value(value, list_item=False, extra=None):
    extra_message = ' [{}]'.format(extra) if extra is not None else ''
    print(' ' * 4, '{}{}{}'.format('- ' if list_item else '', value, extra_message))


def print_definition(key, value, extra=None):
    print_info_key(key, extra)
    print_info_value(value)


def run_info(state):
    def does_exist(path):
        return 'exists' if path.exists() else 'does not exist'

    print_heading("info")
    print_definition(
        'default config file',
        state.paths.default_config_file,
        extra=does_exist(state.paths.default_config_file))
    print_definition(
        'config file used',
        state.paths.config_file,
        extra=does_exist(state.paths.config_file))

    print_info_key('default template search path(s)')
    for path in state.paths.default_template_roots:
        print_info_value(
            path.absolute(),
            list_item=True,
            extra=does_exist(path))

    print_info_key('configured template search path(s)')
    for path in state.configuration['path']:
        print_info_value(
            path.absolute(),
            list_item=True,
            extra=does_exist(path))

    print_info_key('actual template root(s)')
    for path in state.repository.paths:
        print_info_value(
            path.absolute(),
            list_item=True
        )

    print_info_key('number of templates')
    print_info_value('total:    {}'.format(state.repository.numof_templates_total))
    print_info_value('shadowed: {}'.format(state.repository.numof_templates_shadowed))
    print_info_value('visible:  {}'.format(state.repository.numof_templates))

    print_definition('version', TMS_VERSION)


def run_list(state):
    print_heading('List')

    import pprint
    for name, templates in state.repository.templates_by_name():
        template = templates[0]
        print_info_key(name)
        if template.title is not None:
            print_info_value(template.title)
            pprint.pprint(template.properties, indent=2)

    pass


def run_init(state):
    print(state.args)
    template_name = state.args.template_name

    if template_name not in state.repository:
        LOGGER.critical(
            'there is no template named "%s" in the search path',
            template_name
        )
        exit(1)

    template = state.repository.template(template_name)

    # TODO look for logic module
    LOGGER.debug(
        'template "%s" has logic? %s',
        template.name,
        template.has_logic_file()
    )

    if template.has_logic_file():
        # TODO load logic
        logic = None
    else:
        logic = DefaultLogic(template)

    parser = logic.make_parser()
    args = parser.parse_args(state.unkwown_args)

    context = Context(args)
    target = Target(state.args.target_dir)

    logic.create_target(context, target)


def create_parser():
    """ Create the parser for general command line options.
    """
    parser = argparse.ArgumentParser('tms')

    parser.add_argument(
        '-c', '--config',
        type=argparse.FileType('r'),
        default=None,
        help='path to configuration file')

    parser.add_argument(
        '--log-level',
        metavar='LEVEL',
        type=str,
        default='DEBUG')  # TODO: change to WARNING

    subparsers = parser.add_subparsers(dest='subcommand')

    # Sub-command 'list'
    list_parser = subparsers.add_parser('list')

    # Sub-command 'info'
    info_parser = subparsers.add_parser('info')

    # Sub-command 'init'
    init_parser = subparsers.add_parser('init')
    init_parser.add_argument(
        'template_name',
        metavar='TEMPLATE',
        help='template name',
        type=str
    )

    init_parser.add_argument(
        'target_dir',
        metavar='TARGET',
        help='destination directory',
        nargs='?',
        default='.',
        type=str
    )

    return parser


SUB_COMMANDS = {
    'info': run_info,
    'init': run_init,
    'list': run_list,
}


def main():
    parser = create_parser()
    args, unknown_args = parser.parse_known_args(sys.argv[1:])

    if args.subcommand is None:
        LOGGER.critical('no sub command selected')
        parser.print_help()
        exit(0)

    sub_command_name = args.subcommand
    if sub_command_name not in SUB_COMMANDS:
        LOGGER.critical('no such subcommand')
        parser.print_help()
        exit(1)

    # Set the log level
    try:
        LOGGER.setLevel(getattr(logging, args.log_level.upper()))
    except AttributeError:
        LOGGER.error(
            'unknown log-level "%s", defaulting to %s',
            args.log_level.upper(),
            'WARNING')

    # Adjust default configuration file path
    paths = Paths(args.config)

    state = init_state(paths, args, unknown_args)

    SUB_COMMANDS[sub_command_name](state)


if __name__ == '__main__':
    main()
