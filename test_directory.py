#!/usr/bin/env python3

import unittest

from pathlib import Path
import tms

BASE_DIR = Path('proving_ground')


class TestDirectory(unittest.TestCase):
    def test_default_type(self):
        dir = tms.Directory(BASE_DIR)
        self.assertEqual(
            dir.type, tms.DirectoryType.TEMPLATE,
            "a non-configured directory takes the TEMPLATE default type"
        )

    def test_custom_default_type(self):
        dir = tms.Directory(BASE_DIR, tms.DirectoryType.ROOT)
        self.assertEqual(
            dir.type, tms.DirectoryType.ROOT,
            "a non-configured directory takes the given default type"
        )

    def test_malformed_properties(self):
        with self.assertRaises(tms.MalformedPropertiesFile):
            tms.Directory(Path(BASE_DIR, 'malformed-properties-dir'))

    def test_namespace_dir(self):
        dir = tms.Directory(Path(BASE_DIR, 'namespace-dir'))
        self.assertEqual(
            dir.type, tms.DirectoryType.NAMESPACE,
            "directory with .tms-namespace is a namespace"
        )

    def test_root_dir(self):
        dir = tms.Directory(Path(BASE_DIR, 'root-dir'))
        self.assertEqual(
            dir.type, tms.DirectoryType.ROOT,
            "directory with .tms-root is a root"
        )

    def test_ignore_dir(self):
        dir = tms.Directory(Path(BASE_DIR, 'ignore-dir'))
        self.assertEqual(
            dir.type, tms.DirectoryType.IGNORE,
            "directory with .tms-ignore is of type IGNORE"
        )

    def test_does_not_init_on_non_directory(self):
        with self.assertRaises(tms.NoDirectory):
            tms.Directory(Path(BASE_DIR, 'does-not-exist-at-all'))

    def test_malformed_type_name_in_properties(self):
        with self.assertRaises(tms.IllegalPropertyContent):
            tms.Directory(Path(BASE_DIR, 'malformed-dir-type'))

    def test_valid_type_name_in_properties(self):
        dir = tms.Directory(Path(BASE_DIR, 'valid-dir-type'))
        self.assertEqual(
            dir.type, tms.DirectoryType.NAMESPACE,
            ".tms.yml says kind = namespace, then actual type is NAMESPACE"
        )